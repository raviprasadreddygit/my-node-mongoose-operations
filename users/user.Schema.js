
var mongoose = require('mongoose');




var schema = mongoose.Schema;

var  UserSchema = new schema({
        Name : { type : String},
        id:  { type: mongoose.Schema.Types.ObjectId, ref: "id", unique: true },
        Age : {type : Number},
        Class:{type:String},
        Grade: {type:String},
        Created_At : {type:Date, default: Date.now},
        Is_Active :{type:Boolean, default:true}
});

module.exports = mongoose.model('myDbCollectionTest1',UserSchema);